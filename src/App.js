import React, { Component } from 'react'
import Header from './Components/Header/header.jsx'
import './App.css'
import SearchBar from './Components/SearchBar/SearchBar.jsx'
import Issues from './Components/Issues/Issues.jsx'
import Footer from './Components/Footer/Footer.jsx'
import axios from 'axios'
import Paginate from './Components/Paginate/Paginate.jsx'
import linkParser from 'parse-link-header'
import Loader from "react-loader-spinner";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
export class App extends Component {
  state = {
    issues: [],
    loading: false,
    pageNo: 0,
    totalPages: 0,
    perPage: 30,
  }

  updatePage = (page) => {
    if (page >= 0 && page < this.state.totalPages) {
      this.setState({
        pageNo: page,
        issues: []
      })
      this.fetchData(page + 1)
    }
  }
  fetchData = (page = 1) => {
    this.setState({ loading: true })
    axios.get(`https://api.github.com/repos/rails/rails/issues?per_page=${this.state.perPage}&page=${page}`)
      .then(res => {
        console.log(linkParser(res.headers.link).last.page)
        console.log(res.data)
        this.setState({
          issues: res.data,
          totalPages: parseInt(linkParser(res.headers.link).last.page) - 1,
          loading: false
        })
      }).catch(err => {
        this.setState({loading:false})
        console.log(err)
      })
  }
  componentDidMount() {
    this.fetchData()
  }

  render() {
    console.log(this.state.pageNo)
    return (
      <div >
        <Header />
        <SearchBar />
        {this.state.loading ? <div style={{
          display:"flex",
          justifyContent:"center"
        }}> <Loader
        type="ThreeDots"
        color="#00BFFF"
        height={100}
        width={100}
        
      /></div>: (<>
          <Issues data={this.state.issues} />
          <Paginate page={this.state.totalPages} pageNo={this.state.pageNo} updatePage={this.updatePage} />
        </>)}
        <Footer />
      </div>
    )
  }
}

export default App


//sample data

/*
{
        "url": "https://api.github.com/repos/rails/rails/issues/27599",
        "repository_url": "https://api.github.com/repos/rails/rails",
        "labels_url": "https://api.github.com/repos/rails/rails/issues/27599/labels{/name}",
        "comments_url": "https://api.github.com/repos/rails/rails/issues/27599/comments",
        "events_url": "https://api.github.com/repos/rails/rails/issues/27599/events",
        "html_url": "https://github.com/rails/rails/pull/27599",
        "id": 199328180,
        "number": 27599,
        "title": "Fix bug with symbolized keys in .where with nested join (alternative to #27598)",
        "user": {
          "login": "NickLaMuro",
          "id": 314014,
          "avatar_url": "https://avatars.githubusercontent.com/u/314014?v=3",
          "gravatar_id": "",
          "url": "https://api.github.com/users/NickLaMuro",
          "html_url": "https://github.com/NickLaMuro",
          "followers_url": "https://api.github.com/users/NickLaMuro/followers",
          "following_url": "https://api.github.com/users/NickLaMuro/following{/other_user}",
          "gists_url": "https://api.github.com/users/NickLaMuro/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/NickLaMuro/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/NickLaMuro/subscriptions",
          "organizations_url": "https://api.github.com/users/NickLaMuro/orgs",
          "repos_url": "https://api.github.com/users/NickLaMuro/repos",
          "events_url": "https://api.github.com/users/NickLaMuro/events{/privacy}",
          "received_events_url": "https://api.github.com/users/NickLaMuro/received_events",
          "type": "User",
          "site_admin": false
        },
        "labels": [
          {
            "id": 107191,
            "url": "https://api.github.com/repos/rails/rails/labels/activerecord",
            "name": "activerecord",
            "color": "0b02e1",
            "default": false
          },
          {
            "id": 128692,
            "url": "https://api.github.com/repos/rails/rails/labels/needs%20feedback",
            "name": "needs feedback",
            "color": "ededed",
            "default": false
          }
        ],
        "state": "open",
        "locked": false,
        "assignee": {
          "login": "sgrif",
          "id": 1529387,
          "avatar_url": "https://avatars.githubusercontent.com/u/1529387?v=3",
          "gravatar_id": "",
          "url": "https://api.github.com/users/sgrif",
          "html_url": "https://github.com/sgrif",
          "followers_url": "https://api.github.com/users/sgrif/followers",
          "following_url": "https://api.github.com/users/sgrif/following{/other_user}",
          "gists_url": "https://api.github.com/users/sgrif/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/sgrif/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/sgrif/subscriptions",
          "organizations_url": "https://api.github.com/users/sgrif/orgs",
          "repos_url": "https://api.github.com/users/sgrif/repos",
          "events_url": "https://api.github.com/users/sgrif/events{/privacy}",
          "received_events_url": "https://api.github.com/users/sgrif/received_events",
          "type": "User",
          "site_admin": false
        },
        "assignees": [
          {
            "login": "sgrif",
            "id": 1529387,
            "avatar_url": "https://avatars.githubusercontent.com/u/1529387?v=3",
            "gravatar_id": "",
            "url": "https://api.github.com/users/sgrif",
            "html_url": "https://github.com/sgrif",
            "followers_url": "https://api.github.com/users/sgrif/followers",
            "following_url": "https://api.github.com/users/sgrif/following{/other_user}",
            "gists_url": "https://api.github.com/users/sgrif/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/sgrif/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/sgrif/subscriptions",
            "organizations_url": "https://api.github.com/users/sgrif/orgs",
            "repos_url": "https://api.github.com/users/sgrif/repos",
            "events_url": "https://api.github.com/users/sgrif/events{/privacy}",
            "received_events_url": "https://api.github.com/users/sgrif/received_events",
            "type": "User",
            "site_admin": false
          }
        ],
        "milestone": null,
        "comments": 2,
        "created_at": "2017-01-07T01:05:51Z",
        "updated_at": "2017-01-07T01:15:38Z",
        "closed_at": null,
        "pull_request": {
          "url": "https://api.github.com/repos/rails/rails/pulls/27599",
          "html_url": "https://github.com/rails/rails/pull/27599",
          "diff_url": "https://github.com/rails/rails/pull/27599.diff",
          "patch_url": "https://github.com/rails/rails/pull/27599.patch"
        },
        "body": "Summary\n-------\nIn https://github.com/rails/rails/pull/25146, code was added to fix making where clauses against tables with an `enum` column with a `join` present as part of the query.  As part of this fix, it called `singularize` on the `table_name` variable that was passed into the `associated_table` method.\n\n`table_name`, in some circumstances, can also be a symbol if more than one level of joins exists in the Relation (i.e `joins(:book => :subscription)`).  This fixes that by adding chaning the `.stringify_keys!` (found in `ActiveRecord::Relation::WhereClauseFactory`) to be a `.deep_stringify_keys!` to stringfy keys at all levels.\n\n\nOther Information\n-----------------\nThis bug only surfaces when a join is made more than 1 level deep since the `where_clause_builder` calls `stringify_keys!` on the top level of the `.where` hash:\n\nhttps://github.com/rails/rails/blob/21e5fd4/activerecord/lib/active_record/relation/where_clause_factory.rb#L16\n\nSo this hides this edge case from showing up in the test suite with the current coverage and the test that was in PR #25146.\n\nThis is the alternative to https://github.com/rails/rails/pull/27598 in which the change from PR #25146 was fixed in isolation.  Instead, here we fix the false assumption that all `table_name` values being passed into `.associated_table` are a string.  This might have wider effects because of that, so that should be considered when reviewing."
      },
      {
        "url": "https://api.github.com/repos/rails/rails/issues/27598",
        "repository_url": "https://api.github.com/repos/rails/rails",
        "labels_url": "https://api.github.com/repos/rails/rails/issues/27598/labels{/name}",
        "comments_url": "https://api.github.com/repos/rails/rails/issues/27598/comments",
        "events_url": "https://api.github.com/repos/rails/rails/issues/27598/events",
        "html_url": "https://github.com/rails/rails/pull/27598",
        "id": 199327680,
        "number": 27598,
        "title": "Fix bug with symbolized keys in .where with nested join",
        "user": {
          "login": "NickLaMuro",
          "id": 314014,
          "avatar_url": "https://avatars.githubusercontent.com/u/314014?v=3",
          "gravatar_id": "",
          "url": "https://api.github.com/users/NickLaMuro",
          "html_url": "https://github.com/NickLaMuro",
          "followers_url": "https://api.github.com/users/NickLaMuro/followers",
          "following_url": "https://api.github.com/users/NickLaMuro/following{/other_user}",
          "gists_url": "https://api.github.com/users/NickLaMuro/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/NickLaMuro/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/NickLaMuro/subscriptions",
          "organizations_url": "https://api.github.com/users/NickLaMuro/orgs",
          "repos_url": "https://api.github.com/users/NickLaMuro/repos",
          "events_url": "https://api.github.com/users/NickLaMuro/events{/privacy}",
          "received_events_url": "https://api.github.com/users/NickLaMuro/received_events",
          "type": "User",
          "site_admin": false
        },
        "labels": [
          {
            "id": 107191,
            "url": "https://api.github.com/repos/rails/rails/labels/activerecord",
            "name": "activerecord",
            "color": "0b02e1",
            "default": false
          },
          {
            "id": 128692,
            "url": "https://api.github.com/repos/rails/rails/labels/needs%20feedback",
            "name": "needs feedback",
            "color": "ededed",
            "default": false
          }
        ],
        "state": "open",
        "locked": false,
        "assignee": {
          "login": "sgrif",
          "id": 1529387,
          "avatar_url": "https://avatars.githubusercontent.com/u/1529387?v=3",
          "gravatar_id": "",
          "url": "https://api.github.com/users/sgrif",
          "html_url": "https://github.com/sgrif",
          "followers_url": "https://api.github.com/users/sgrif/followers",
          "following_url": "https://api.github.com/users/sgrif/following{/other_user}",
          "gists_url": "https://api.github.com/users/sgrif/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/sgrif/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/sgrif/subscriptions",
          "organizations_url": "https://api.github.com/users/sgrif/orgs",
          "repos_url": "https://api.github.com/users/sgrif/repos",
          "events_url": "https://api.github.com/users/sgrif/events{/privacy}",
          "received_events_url": "https://api.github.com/users/sgrif/received_events",
          "type": "User",
          "site_admin": false
        },
        "assignees": [
          {
            "login": "sgrif",
            "id": 1529387,
            "avatar_url": "https://avatars.githubusercontent.com/u/1529387?v=3",
            "gravatar_id": "",
            "url": "https://api.github.com/users/sgrif",
            "html_url": "https://github.com/sgrif",
            "followers_url": "https://api.github.com/users/sgrif/followers",
            "following_url": "https://api.github.com/users/sgrif/following{/other_user}",
            "gists_url": "https://api.github.com/users/sgrif/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/sgrif/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/sgrif/subscriptions",
            "organizations_url": "https://api.github.com/users/sgrif/orgs",
            "repos_url": "https://api.github.com/users/sgrif/repos",
            "events_url": "https://api.github.com/users/sgrif/events{/privacy}",
            "received_events_url": "https://api.github.com/users/sgrif/received_events",
            "type": "User",
            "site_admin": false
          }
        ],
        "milestone": null,
        "comments": 3,
        "created_at": "2017-01-07T01:00:48Z",
        "updated_at": "2017-01-07T01:15:33Z",
        "closed_at": null,
        "pull_request": {
          "url": "https://api.github.com/repos/rails/rails/pulls/27598",
          "html_url": "https://github.com/rails/rails/pull/27598",
          "diff_url": "https://github.com/rails/rails/pull/27598.diff",
          "patch_url": "https://github.com/rails/rails/pull/27598.patch"
        },
        "body": "Summary\n-------\nIn https://github.com/rails/rails/pull/25146, code was added to fix making where clauses against tables with an `enum` column with a `join` present as part of the query.  As part of this fix, it called `singularize` on the `table_name` variable that was passed into the `associated_table` method.\n\n`table_name`, in some circumstances, can also be a symbol if more than one level of joins exists in the Relation (i.e `joins(:book => :subscription)`).  This fixes that by adding `.to_s` before calling `.singularize` on the `table_name` variable.\n\n\nOther Information\n-----------------\nThis bug only surfaces when a join is made more than 1 level deep since the `where_clause_builder` calls `stringify_keys!` on the top level of the `.where` hash:\n\nhttps://github.com/rails/rails/blob/21e5fd4/activerecord/lib/active_record/relation/where_clause_factory.rb#L16\n\nSo this hides this edge case from showing up in the test suite with the current coverage and the test that was in PR #25146.\n\nThe other solution to this problem is to deeply stringify the keys in the `where_clause_builder` and all method calls following that can safely assume strings are being passed in as keys.  This is a heavier hammer, but the assumption is already being made on the top level to do this, so it seems like a better place to put it instead of scattered throughout the codebase and having it handle both strings/symbols in multiple places.  This alternative will be proposed in a separate PR.\n\n\nAlso of note, the this probably isn't the best place for a test like this, but it was the simplest way I could get a test in place without familiarizing myself with the entire ActiveRecord test suite (copying what was done in the previous PR).  Suggestions welcome for a better place for this test to live, but it is worth noting that this same test will also be used to confirm the same working functionality in the alternative form for this PR."
      },
      {
        "url": "https://api.github.com/repos/rails/rails/issues/27597",
        "repository_url": "https://api.github.com/repos/rails/rails",
        "labels_url": "https://api.github.com/repos/rails/rails/issues/27597/labels{/name}",
        "comments_url": "https://api.github.com/repos/rails/rails/issues/27597/comments",
        "events_url": "https://api.github.com/repos/rails/rails/issues/27597/events",
        "html_url": "https://github.com/rails/rails/pull/27597",
        "id": 199307639,
        "number": 27597,
        "title": "Consistency between first() and last() with limit",
        "user": {
          "login": "brchristian",
          "id": 2460418,
          "avatar_url": "https://avatars.githubusercontent.com/u/2460418?v=3",
          "gravatar_id": "",
          "url": "https://api.github.com/users/brchristian",
          "html_url": "https://github.com/brchristian",
          "followers_url": "https://api.github.com/users/brchristian/followers",
          "following_url": "https://api.github.com/users/brchristian/following{/other_user}",
          "gists_url": "https://api.github.com/users/brchristian/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/brchristian/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/brchristian/subscriptions",
          "organizations_url": "https://api.github.com/users/brchristian/orgs",
          "repos_url": "https://api.github.com/users/brchristian/repos",
          "events_url": "https://api.github.com/users/brchristian/events{/privacy}",
          "received_events_url": "https://api.github.com/users/brchristian/received_events",
          "type": "User",
          "site_admin": false
        },
        "labels": [
          {
            "id": 107191,
            "url": "https://api.github.com/repos/rails/rails/labels/activerecord",
            "name": "activerecord",
            "color": "0b02e1",
            "default": false
          }
        ],
        "state": "open",
        "locked": false,
        "assignee": {
          "login": "pixeltrix",
          "id": 6321,
          "avatar_url": "https://avatars.githubusercontent.com/u/6321?v=3",
          "gravatar_id": "",
          "url": "https://api.github.com/users/pixeltrix",
          "html_url": "https://github.com/pixeltrix",
          "followers_url": "https://api.github.com/users/pixeltrix/followers",
          "following_url": "https://api.github.com/users/pixeltrix/following{/other_user}",
          "gists_url": "https://api.github.com/users/pixeltrix/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/pixeltrix/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/pixeltrix/subscriptions",
          "organizations_url": "https://api.github.com/users/pixeltrix/orgs",
          "repos_url": "https://api.github.com/users/pixeltrix/repos",
          "events_url": "https://api.github.com/users/pixeltrix/events{/privacy}",
          "received_events_url": "https://api.github.com/users/pixeltrix/received_events",
          "type": "User",
          "site_admin": false
        },
        "assignees": [
          {
            "login": "pixeltrix",
            "id": 6321,
            "avatar_url": "https://avatars.githubusercontent.com/u/6321?v=3",
            "gravatar_id": "",
            "url": "https://api.github.com/users/pixeltrix",
            "html_url": "https://github.com/pixeltrix",
            "followers_url": "https://api.github.com/users/pixeltrix/followers",
            "following_url": "https://api.github.com/users/pixeltrix/following{/other_user}",
            "gists_url": "https://api.github.com/users/pixeltrix/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/pixeltrix/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/pixeltrix/subscriptions",
            "organizations_url": "https://api.github.com/users/pixeltrix/orgs",
            "repos_url": "https://api.github.com/users/pixeltrix/repos",
            "events_url": "https://api.github.com/users/pixeltrix/events{/privacy}",
            "received_events_url": "https://api.github.com/users/pixeltrix/received_events",
            "type": "User",
            "site_admin": false
          }
        ],
        "milestone": null,
        "comments": 1,
        "created_at": "2017-01-06T22:34:28Z",
        "updated_at": "2017-01-06T22:46:24Z",
        "closed_at": null,
        "pull_request": {
          "url": "https://api.github.com/repos/rails/rails/pulls/27597",
          "html_url": "https://github.com/rails/rails/pull/27597",
          "diff_url": "https://github.com/rails/rails/pull/27597.diff",
          "patch_url": "https://github.com/rails/rails/pull/27597.patch"
        },
        "body": "Fixes #23979.\r\n\r\nAs discussed in #23979, there was an inconsistency between the way that `first()` and `last()` would interact with `limit`. Specifically:\r\n\r\n```Ruby\r\n> Topic.limit(1).first(2).size\r\n=> 2\r\n> Topic.limit(1).last(2).size\r\n=> 1\r\n```\r\n\r\nThis PR is a refactor and rebase of #24124, with a simpler test suite and simpler implementation.\r\n\r\nDiscussion with Rails community members as well as DHH in https://github.com/rails/rails/pull/23598#issuecomment-189675440 showed that the behavior or `first` should be brought into line with `last` (rather than vice-versa).\r\n\r\nThis PR resolves the inconsistency between `first` and `last` when used in conjunction with `limit`.\r\n"
      },
      {
        "url": "https://api.github.com/repos/rails/rails/issues/27595",
        "repository_url": "https://api.github.com/repos/rails/rails",
        "labels_url": "https://api.github.com/repos/rails/rails/issues/27595/labels{/name}",
        "comments_url": "https://api.github.com/repos/rails/rails/issues/27595/comments",
        "events_url": "https://api.github.com/repos/rails/rails/issues/27595/events",
        "html_url": "https://github.com/rails/rails/issues/27595",
        "id": 199262959,
        "number": 27595,
        "title": "has_many :through with `include Comparable`",
        "user": {
          "login": "jnimety",
          "id": 70484,
          "avatar_url": "https://avatars.githubusercontent.com/u/70484?v=3",
          "gravatar_id": "",
          "url": "https://api.github.com/users/jnimety",
          "html_url": "https://github.com/jnimety",
          "followers_url": "https://api.github.com/users/jnimety/followers",
          "following_url": "https://api.github.com/users/jnimety/following{/other_user}",
          "gists_url": "https://api.github.com/users/jnimety/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/jnimety/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/jnimety/subscriptions",
          "organizations_url": "https://api.github.com/users/jnimety/orgs",
          "repos_url": "https://api.github.com/users/jnimety/repos",
          "events_url": "https://api.github.com/users/jnimety/events{/privacy}",
          "received_events_url": "https://api.github.com/users/jnimety/received_events",
          "type": "User",
          "site_admin": false
        },
        "labels": [
          {
            "id": 107191,
            "url": "https://api.github.com/repos/rails/rails/labels/activerecord",
            "name": "activerecord",
            "color": "0b02e1",
            "default": false
          },
          {
            "id": 128692,
            "url": "https://api.github.com/repos/rails/rails/labels/needs%20feedback",
            "name": "needs feedback",
            "color": "ededed",
            "default": false
          },
          {
            "id": 107186,
            "url": "https://api.github.com/repos/rails/rails/labels/regression",
            "name": "regression",
            "color": "e10c02",
            "default": false
          },
          {
            "id": 126910270,
            "url": "https://api.github.com/repos/rails/rails/labels/With%20reproduction%20steps",
            "name": "With reproduction steps",
            "color": "009800",
            "default": false
          }
        ],
        "state": "open",
        "locked": false,
        "assignee": null,
        "assignees": [

        ],
        "milestone": null,
        "comments": 9,
        "created_at": "2017-01-06T18:57:48Z",
        "updated_at": "2017-01-06T22:59:05Z",
        "closed_at": null,
        "body": "I'm not sure this is a bug but this has worked in previous versions of rails and stopped working in rails 5.0.1. It was a huge pain to track down so if nothing else maybe someone will find this if they're running into a similar issue.\r\n\r\nIf a has_many :through join model includes Comparable then creating join records through the association does not set the foreign key for all but the first record. For example, in the test below, `Event.create(user_ids: [user1.id, user2.id])` will create two Invite join records but only the first one is assigned an event_id.\r\n \r\n### Steps to reproduce\r\n\r\n```\r\nbegin\r\n  require \"bundler/inline\"\r\nrescue LoadError => e\r\n  $stderr.puts \"Bundler version 1.10 or later is required. Please update your Bundler\"\r\n  raise e\r\nend\r\n\r\ngemfile(true) do\r\n  source \"https://rubygems.org\"\r\n  gem \"activerecord\", \"5.0.1\"\r\n  #gem \"activerecord\", \"5.0.0.1\"\r\n  gem \"pg\"\r\nend\r\n\r\nrequire \"active_record\"\r\nrequire \"minitest/autorun\"\r\nrequire \"logger\"\r\n\r\n# This connection will do for database-independent bug reports.\r\nActiveRecord::Base.establish_connection adapter: \"postgresql\"\r\nActiveRecord::Base.connection.drop_database \"test_has_many_through_with_comparable\"\r\nActiveRecord::Base.connection.create_database \"test_has_many_through_with_comparable\"\r\nActiveRecord::Base.establish_connection adapter: \"postgresql\", database: \"test_has_many_through_with_comparable\"\r\nActiveRecord::Base.logger = Logger.new(STDOUT)\r\n\r\nActiveRecord::Schema.define do\r\n  create_table :events, force: true do |t|\r\n  end\r\n\r\n  create_table :invites, force: true do |t|\r\n    t.integer :event_id\r\n    t.integer :user_id\r\n  end\r\n\r\n  create_table :users, force: true do |t|\r\n  end\r\nend\r\n\r\nclass Event < ActiveRecord::Base\r\n  has_many :invites\r\n\r\n  has_many :users, :through => :invites\r\nend\r\n\r\nclass Invite < ActiveRecord::Base\r\n  include Comparable\r\n\r\n  belongs_to :event\r\n  belongs_to :user\r\nend\r\n\r\nclass User < ActiveRecord::Base\r\nend\r\n\r\nclass BugTest < Minitest::Test\r\n  def test_has_many_through_with_comparable\r\n    user1 = User.create\r\n    user2 = User.create\r\n\r\n    event = Event.create(user_ids: [user1.id, user2.id]) # Invite#event_id does not get set for user2\r\n\r\n    assert_equal event.invites.count, 2\r\n  end\r\nend\r\n```\r\n\r\n### Expected behavior\r\n\r\nCorrectly create joins records with foreign_keys correctly populated\r\n\r\n### Actual behavior\r\n\r\nforeign_keys are nil\r\n\r\n### System configuration\r\n**Rails version**: 5.0.1\r\n\r\n**Ruby version**: 2.3.3\r\n"
      }
*/