import React, { Component } from 'react'
import './paginate.css'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
export class Paginate extends Component {
    render() {
        let arr = [...Array(this.props.page)]
        let use = []
        let main = []
        console.log(this.props)
        if (this.props.page <= 12) {
            for (let i = 0; i < this.props.page; i++) {
                main.push(i)
            }
        }
        else {
            for (let i = 2; i < arr.length - 2; i++) {
                arr[i] = '.'
            }

            let c = 0
            for (let i = this.props.pageNo - 1; i >= 0; i--) {
                if (c === 3) {
                    break;
                }
                arr[i] = i
                c++
            }
            c = 0
            for (let i = this.props.pageNo; i < arr.length; i++) {
                if (c === 3) {
                    break;
                }
                arr[i] = i
                c++
            }


            for (let i = 0; i < arr.length; i++) {
                if (!arr[i] || arr[i] !== '.') {
                    use.push(i)
                }
            }
            main.push(use[0])
            for (let i = 1; i < use.length; i++) {
                if (use[i] - 1 !== use[i - 1]) {
                    main.push('...')
                }
                main.push(use[i])
            }
        }

        return (
            <div className="paginateCont">
                <div className="pageBtn" style={{
                    display: "flex",
                    alignItems: "center",
                    color: "#0366d6"
                }} onClick={() => this.props.updatePage(this.props.pageNo - 1)}>
                    <ArrowBackIosIcon /> Prev
                </div>
                <div className="btn-num">
                    {
                        main.map((ele, i) => {
                            return (
                                ele === '...' ? <div key={i} style={{
                                    padding: "10px 12px"
                                }}>...</div>
                                    : this.props.pageNo === ele ? <div key={i} className="pageSelectBtn">{ele + 1}</div>
                                        : <div key={i} className="pageBtn" onClick={() => this.props.updatePage(ele)}>{ele + 1}</div>
                            )
                        })
                    }
                </div>
                <div className="pageBtn" style={{
                    display: "flex",
                    alignItems: "center",
                    color: "#0366d6",
                }} onClick={() => this.props.updatePage(this.props.pageNo + 1)}>
                    Next <ArrowForwardIosIcon />
                </div>
            </div>
        )
    }
}

export default Paginate
