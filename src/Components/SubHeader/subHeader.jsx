import React, { Component } from 'react'
import './subHeader.css'

import { ReactComponent as Bus } from '../../images/bus.svg'
import { ReactComponent as Issue } from '../../images/issue.svg'
import { ReactComponent as Pull } from '../../images/pull.svg'

import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ForumRoundedIcon from '@material-ui/icons/ForumRounded';
import CodeIcon from '@material-ui/icons/Code';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';

export class SubHeader extends Component {
    render() {
        return (
            <div className="subHeader">
                <div className="row1">
                    <div style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center"
                    }} >
                        <Bus fill="black" />
                        <span style={{ marginLeft: "10px" }}>facebook / create-react-app </span>
                    </div>
                    <div className="btns">
                        <button className="subHeaderBtn">
                            <FavoriteBorderIcon className="btnIcon" />
                            <span>Sponsor</span>
                        </button>
                        <button className="subHeaderBtn">
                            <span>Watch</span>
                            <ArrowDropDownIcon style={{ color: "black", marginLeft: '4px' }} />
                        </button>
                        <button className="subHeaderBtn">
                            <FavoriteBorderIcon className="btnIcon" />
                            <span>Star</span>
                        </button>
                        <button className="subHeaderBtn">
                            <FavoriteBorderIcon className="btnIcon" />
                            <span>Fork</span>
                        </button>

                    </div>

                </div>
                <div style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    width: '100%'
                }}>
                    <div className="row2">
                        <div >
                            <CodeIcon className="navIcons" /> <div>Code</div>
                        </div>
                        <div >
                            <Issue className="navIcons" style={{ fill: 'black' }} /> <div>Issues</div>
                        </div>
                        <div >
                            <Pull className="navIcons" /> <div>Pull Requests</div>
                        </div>
                        <div >
                            <ForumRoundedIcon className="navIcons" /> <div>Discussions</div>
                        </div>
                        <div >
                            <ForumRoundedIcon className="navIcons" /> <div>Actions</div>
                        </div>
                        <div >
                            <ForumRoundedIcon className="navIcons" /> <div>Projects</div>
                        </div>
                        <div >
                            <ForumRoundedIcon className="navIcons" /> <div>Security</div>
                        </div>
                        <div >
                            <ForumRoundedIcon className="navIcons" /> <div>Insights</div>
                        </div>
                    </div>
                    <div className="iconMenu">
                        <MoreHorizIcon />
                    </div>
                </div>

            </div>
        )
    }
}

export default SubHeader
