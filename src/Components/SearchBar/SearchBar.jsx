import React, { Component } from 'react'
import './searchBar.css'
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import SearchIcon from '@material-ui/icons/Search';
import { ReactComponent as Label } from '../../images/label.svg'
import { ReactComponent as Mile } from '../../images/mile.svg'
export class SearchBar extends Component {
    state = {
        value: "is:issue is:open "
    }
    render() {
        return (
            <div className="container">
                <div style={{
                    display:"flex",
                    width:"100%"
                }}>
                    <button className="filterBtn"><p>Filter</p><ArrowDropDownIcon /></button>
                    <div className="searchInputDiv">
                        <SearchIcon className="searchIcon" /> <input className="searchInput" value={this.state.value} onChange={(e) => this.setState({ value: e.target.value })} />
                    </div>
                </div>
                <div className="searchBtnStyle">
                    <div style={{
                        display:"flex"
                    }}>
                    <button className="subHeaderBtn" style={{
                        margin: 0,
                        borderTopRightRadius: 0,
                        borderBottomRightRadius: 0
                    }}>
                        <Label style={{ fill: "black", marginRight: '4px' }} />
                        <span style={{
                            fontWeight: '500',
                            lineHeight: '20px',
                            fontSize: '14px'
                        }}>Labels</span>
                    </button>
                    <button className="subHeaderBtn" style={{
                        borderTopLeftRadius: 0,
                        WebkitBorderBottomLeftRadius: 0
                    }} >

                        <Mile style={{ color: "black", marginRight: '4px' }} />
                        <span style={{
                            fontWeight: '500',
                            lineHeight: '20px',
                            fontSize: '14px'
                        }}>Milestones</span>
                    </button>
                    </div>
                    <button className="greenBtn">
                        New Issue
                </button>
                </div>
            </div>
        )
    }
}

export default SearchBar
