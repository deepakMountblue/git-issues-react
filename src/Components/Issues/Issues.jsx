import React, { Component } from 'react'
import './issue.css'
import { ReactComponent as Issue } from '../../images/issue.svg'
import CheckIcon from '@material-ui/icons/Check';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
export class Issues extends Component {

    shorten = (text = "", maxLength = 140) => {
        // Normalize newlines
        let cleanText = text.replace(/\\r\\n/g, "\n");

        // Return if short enough already
        if (cleanText.length <= maxLength) {
            return cleanText;
        }

        const ellip = " ...";

        // Return the 140 chars as-is if they end in a non-word char
        const oneTooLarge = cleanText.substr(0, 141);
        if (/\W$/.test(oneTooLarge)) {
            return oneTooLarge.substr(0, 140) + ellip;
        }

        // Walk backwards to the nearest non-word character
        let i = oneTooLarge.length;
        while (--i) {
            if (/\W/.test(oneTooLarge[i])) {
                return oneTooLarge.substr(0, i) + ellip;
            }
        }
        console.log(oneTooLarge.substr(0, 140) + ellip)
        return oneTooLarge.substr(0, 140) + ellip;
    }
    insertMentionLinks = (markdown) => {
        return markdown.replace(/\B(@([a-zA-Z0-9](-?[a-zA-Z0-9_])+))/g, `**[$1](https://github.com/$2)**`);
    }

    
    hexToRgb = hex =>
        hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i
            , (m, r, g, b) => '#' + r + r + g + g + b + b)
            .substring(1).match(/.{2}/g)
            .map(x => parseInt(x, 16))
    render() {
        return (
            <div className="mainCont">
                <div className="listContainer">
                    <div className="head">
                        <div className="openHide">
                            <div className="headText" style={{
                                color: "black"
                            }}>
                                <Issue style={{ marginRight: '10px' }} />
                                <h3>0 Open</h3>
                            </div>
                            <div className="headText">
                                <CheckIcon style={{ marginRight: '10px' }} />
                                <p>0 Closed</p>
                            </div>
                        </div>
                        <div className="listheaderHide">
                            <div className="headText">
                                <p>Author</p>
                                <ArrowDropDownIcon style={{ marginRight: '10px' }} />
                            </div>
                            <div className="headText">
                                <p>Label</p>
                                <ArrowDropDownIcon style={{ marginRight: '10px' }} />
                            </div>
                            <div className="headText">
                                <p>Project</p>
                                <ArrowDropDownIcon style={{ marginRight: '10px' }} />
                            </div>
                            <div className="headText">
                                <p>Milestones</p>
                                <ArrowDropDownIcon style={{ marginRight: '10px' }} />
                            </div>
                            <div className="headText">
                                <p>Assignee</p>
                                <ArrowDropDownIcon style={{ marginRight: '10px' }} />
                            </div>
                            <div className="headText">
                                <p>Sort</p>
                                <ArrowDropDownIcon style={{ marginRight: '10px' }} />
                            </div>
                        </div>
                    </div>
                    {this.props.data.length === 0 && (<div className="listBody">
                        <div className="nothing">
                            <Issue style={{ transform: 'scale(1.4)' }} />
                            <h1 style={{
                                fontSize: '24px',
                                margin: "16px 0"
                            }}>
                                No results matched your search.</h1>
                            <p style={{
                                fontSize: "16px",
                                color: '#586069'
                            }}>You could search all of GitHub or try an advanced search.</p>
                        </div>
                    </div>)}
                    {this.props.data.length > 0 && (
                        <div className="allLists">
                            {this.props.data.map((ele, i) => {
                                const d = Date.now() - new Date(ele.updated_at)
                                let weekdays = Math.floor(d / 1000 / 60 / 60 / 24 / 7);
                                let days = Math.floor(d / 1000 / 60 / 60 / 24 - weekdays * 7);
                                let hours = Math.floor(d / 1000 / 60 / 60 - weekdays * 7 * 24 - days * 24);
                                let minutes = Math.floor(d / 1000 / 60 - weekdays * 7 * 24 * 60 - days * 24 * 60 - hours * 60);
                                return (<div key={ele.id} style={{
                                    padding: '15px 0',
                                    display: "flex",
                                    borderBottom: i === this.props.data.length - 1 ? '' : '1px solid #e1e4e8',
                                    alignItems:"center",
                                    flexWrap:'warp',

                                }} className="hoverList">
                                    <Issue fill="green" style={{
                                        marginLeft: "20px"
                                    }} />
                                    <div style={{ margin: "0 10px" }}>
                                        <div className="issueMedia">
                                            <h2 style={{fontSize:"16px",wordBreak:"break-all",wordWrap:"break-word"}}>{this.shorten(ele.title)}</h2>
                                            <div style={{
                                                display: "flex",
                                                flexWrap:"wrap",
                                               
                                                
                                            }}>
                                                {ele.labels.map((element, index) => {
                                                    const styles = {
                                                        backgroundColor: "#"+element.color,
                                                        border:'none',
                                                        padding:'4px',
                                                        color:"black",
                                                        borderRadius:"10px",
                                                        marginLeft:'10px',

                                                    }
                                                    return (
                                                        <button key={index} style={styles} >
                                                            {element.name}
                                                        </button>
                                                    )
                                                })}
                                            </div>
                                        </div>

                                        <p style={{
                                            color: "#586069",
                                            fontSize: "12px",
                                            marginTop: "4px",
                                            lineHeight: "1.5",
                                            wordWrap:"break-word",
                                            wordBreak:"break-all"
                                        }}>#{ele.number} opened {days > 0 ? days + " days" : hours > 0 ? hours + " hours" : minutes > 0 ? minutes + " minutes" : ""} ago </p>
                                    </div>
                                   {ele.comments!==0&& <div style={{display:"flex",padding:"0 10px",marginLeft:'auto'}}>
                                        <ChatBubbleOutlineIcon/><p>{ele.comments}</p>
                                    </div>}
                                </div>)
                            })}
                        </div>
                    )}
                </div>
            </div >

        )
    }
}

export default Issues
