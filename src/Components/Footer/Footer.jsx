import React, { Component } from 'react'
import './footer.css'
import { ReactComponent as Logo } from '../../images/gitLogo.svg'
export class Footer extends Component {
    render() {
        return (
            <footer>
                <div className="footer-row" >
                    <p>© 2021 GitHub, Inc.</p>
                    <p style={{ color: 'blue' }}>Terms</p>
                    <p style={{ color: 'blue' }}>Privacy</p>
                    <p style={{ color: 'blue' }}>Security</p>
                    <p style={{ color: 'blue' }}>Status</p>
                    <p style={{ color: 'blue' }}>Docs</p>
                </div>
                <div className="footer-logo"><Logo fill="#d1d5da" /></div>
                <div className="footer-row" >
                   
                    <p style={{ color: 'blue' }}>Contact Github</p>
                    <p style={{ color: 'blue' }}>Pricing</p>
                    <p style={{ color: 'blue' }}>Api</p>
                    <p style={{ color: 'blue' }}>Traning</p>
                    <p style={{ color: 'blue' }}>Blog</p>
                    <p style={{ color: 'blue' }}>About</p>
                </div>
            </footer>
        )
    }
}

export default Footer
