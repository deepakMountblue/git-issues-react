import React, { Component } from 'react'
import './header.css'

import {ReactComponent as Logo} from '../../images/gitLogo.svg'
import {ReactComponent as Bell} from '../../images/bell.svg'
import MenuIcon from '@material-ui/icons/Menu'

import SubHeader from '../SubHeader/subHeader.jsx'

export class Header extends Component {
    render() {
        return (
            <>
           <header style={{
               background:"#24292e"
           }}>
             <MenuIcon style={{color:'white'}}/>
             <Logo fill="white" />
             <Bell fill="white" />
           </header>
           <SubHeader />
           </>
        )
    }
}

export default Header
